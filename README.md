# Transportation api

## Usage
To start tests `./gradlew test`
To run `../gradlew bootJar`
To build `../gradlew buildJar`

## Curl examples
Is delayed api - `curl http://localhost:8081/lines/M4`

Get line in time and coordinated - `curl http://localhost:8081/lines?x=1&y=1&time=10:00:00`

## Notes
The app reads data from provided csv files located in `src/main/resources/data` directory. 
If you want to run it with different set of data - please replace it there and rerun or rebuild.

For simplicity all data loaded in memory on app start. 
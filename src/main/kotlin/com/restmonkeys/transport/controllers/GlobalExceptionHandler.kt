package com.restmonkeys.transport.controllers

import com.restmonkeys.transport.exceptions.MonoException
import com.restmonkeys.transport.utils.mark
import com.restmonkeys.transport.utils.then
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

@RestController
@ControllerAdvice
class GlobalExceptionHandler {

    companion object {
        private val log = LoggerFactory.getLogger(GlobalExceptionHandler::class.java)
    }


    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(Exception::class)
    fun handleUnknownType(e: Exception): Map<String, String?> {
        log.info(mark("errorMessage" to e.message), "Error happened")
        return mapOf("message" to e.message)
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MonoException::class)
    fun handleError(e: MonoException): Mono<String> {
        return e.mono
                .then { log.info(mark("errorMessage" to it.toString()), "Error happened") }
                .map { it.toString() }
    }

}

package com.restmonkeys.transport.controllers

import com.restmonkeys.transport.services.LinesService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@RestController
class LinesController(val linesService: LinesService) {

    @GetMapping("/lines")
    fun lines(@RequestParam x: Int, @RequestParam y: Int, @RequestParam time: String): Flux<String> {
        return linesService.lineAt(time, x to y).map { it.name }
    }

    @GetMapping("/lines/{name}")
    fun isDelayed(@PathVariable name: String): Mono<Boolean> {
        return linesService.isDelayed(name)
    }
}
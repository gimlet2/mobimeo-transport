package com.restmonkeys.transport.dto

data class Delay(val line: String, val delay: Int)
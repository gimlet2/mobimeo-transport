package com.restmonkeys.transport.dto

data class Line(val id: Int, val name: String)
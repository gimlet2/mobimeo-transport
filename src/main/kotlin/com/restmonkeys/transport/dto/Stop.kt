package com.restmonkeys.transport.dto

data class Stop(val id: Int, val x: Int, val y: Int)
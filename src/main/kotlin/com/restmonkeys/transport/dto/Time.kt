package com.restmonkeys.transport.dto

data class Time(val lineId: Int, val stopId: Int, val time: String)
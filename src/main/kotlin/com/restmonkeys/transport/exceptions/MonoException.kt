package com.restmonkeys.transport.exceptions

import reactor.core.publisher.Mono

open class MonoException(val mono: Mono<Any>) : Exception()

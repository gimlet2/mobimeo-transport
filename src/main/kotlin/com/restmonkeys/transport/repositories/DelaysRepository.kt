package com.restmonkeys.transport.repositories

import com.opencsv.CSVReaderHeaderAware
import com.restmonkeys.transport.dto.Delay
import com.restmonkeys.transport.dto.Line
import org.springframework.stereotype.Repository
import reactor.core.publisher.Mono
import reactor.core.publisher.toFlux
import reactor.core.publisher.toMono
import java.nio.file.Files
import java.nio.file.Paths

@Repository
class DelaysRepository {

    companion object {
        private val data = CSVReaderHeaderAware(
                Files.newBufferedReader(Paths.get(ClassLoader.getSystemResource("data/delays.csv").toURI())))
                .readAll()
                .map { Delay(it[0], it[1].toInt()) }
                .toFlux()
    }

    fun getDelayByLine(line: Line): Mono<Delay> {
        return data.filter { it.line == line.name }.toMono()
    }

}
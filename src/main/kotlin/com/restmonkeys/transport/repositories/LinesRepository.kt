package com.restmonkeys.transport.repositories

import com.opencsv.CSVReaderHeaderAware
import com.restmonkeys.transport.dto.Line
import org.springframework.stereotype.Repository
import reactor.core.publisher.Mono
import reactor.core.publisher.toFlux
import reactor.core.publisher.toMono
import java.nio.file.Files
import java.nio.file.Paths

@Repository
class LinesRepository {

    companion object {
        private val data = CSVReaderHeaderAware(
                Files.newBufferedReader(Paths.get(ClassLoader.getSystemResource("data/lines.csv").toURI())))
                .readAll()
                .map { Line(it[0].toInt(), it[1]) }
                .toFlux()
    }

    fun getLineById(id: Int): Mono<Line> {
        return data.filter { it.id == id }.toMono()
    }

    fun getLineByName(name: String): Mono<Line> {
        return data.filter { it.name == name }.toMono()
    }

}
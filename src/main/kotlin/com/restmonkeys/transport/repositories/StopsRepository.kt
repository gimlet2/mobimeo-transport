package com.restmonkeys.transport.repositories

import com.opencsv.CSVReaderHeaderAware
import com.restmonkeys.transport.dto.Stop
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.toFlux
import java.nio.file.Files
import java.nio.file.Paths

@Repository
class StopsRepository {

    companion object {
        private val data = CSVReaderHeaderAware(
                Files.newBufferedReader(Paths.get(ClassLoader.getSystemResource("data/stops.csv").toURI())))
                .readAll()
                .map { Stop(it[0].toInt(), it[1].toInt(), it[2].toInt()) }
                .toFlux()
    }

    fun getStopsByCoordinates(coordinates: Pair<Int, Int>): Flux<Stop> {
        return data.filter { it.x == coordinates.first && it.y == coordinates.second }
    }

}
package com.restmonkeys.transport.repositories

import com.opencsv.CSVReaderHeaderAware
import com.restmonkeys.transport.dto.Stop
import com.restmonkeys.transport.dto.Time
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.toFlux
import java.nio.file.Files
import java.nio.file.Paths

@Repository
class TimesRepository {

    companion object {
        private val data = CSVReaderHeaderAware(
                Files.newBufferedReader(Paths.get(ClassLoader.getSystemResource("data/times.csv").toURI())))
                .readAll()
                .map { Time(it[0].toInt(), it[1].toInt(), it[2]) }
                .toFlux()
    }

    fun getLineIdByStopAndTime(stop: Stop, time: String): Flux<Int> {
        return data.filter { it.stopId == stop.id && it.time == time }.map { it.lineId }
    }

}
package com.restmonkeys.transport.services

import com.restmonkeys.transport.dto.Line
import com.restmonkeys.transport.repositories.DelaysRepository
import com.restmonkeys.transport.repositories.LinesRepository
import com.restmonkeys.transport.repositories.StopsRepository
import com.restmonkeys.transport.repositories.TimesRepository
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Service
class LinesService(
        val linesRepository: LinesRepository,
        val stopsRepository: StopsRepository,
        val delaysRepository: DelaysRepository,
        val timesRepository: TimesRepository
) {

    fun lineAt(time: String, coordinates: Pair<Int, Int>): Flux<Line> {
        return stopsRepository.getStopsByCoordinates(coordinates)
                .flatMap { timesRepository.getLineIdByStopAndTime(it, time) }
                .flatMap { linesRepository.getLineById(it) }
    }

    fun isDelayed(lineName: String): Mono<Boolean> {
        return linesRepository.getLineByName(lineName)
                .flatMap { delaysRepository.getDelayByLine(it) }
                .map { it.delay != 0 }
                .defaultIfEmpty(false)
    }

}

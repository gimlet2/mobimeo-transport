package com.restmonkeys.transport.utils

import net.logstash.logback.marker.LogstashMarker
import net.logstash.logback.marker.Markers

fun mark(vararg entries: Pair<String,*>): LogstashMarker = Markers.appendEntries(entries.toMap())
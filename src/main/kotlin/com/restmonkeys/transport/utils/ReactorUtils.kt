package com.restmonkeys.transport.utils

import reactor.core.publisher.Mono


inline fun <T> Mono<T>.then(crossinline action: (T) -> Unit): Mono<T> {
    return map {
        action(it)
        it
    }
}
package com.restmonkeys.transport.repositories

import com.restmonkeys.transport.dto.Line
import org.amshove.kluent.`should be equal to`
import org.junit.Before
import org.junit.Test

/**
 * Created by Andrei Chernyshev on 8/19/18.
 */
class DelaysRepositoryTest {

    lateinit var repository: DelaysRepository

    @Before
    fun setup() {
        repository = DelaysRepository()
    }

    @Test
    fun getDelayByLine() {
        val result = repository.getDelayByLine(Line(0, "M4")).block()!!
        result.delay `should be equal to` 1
        result.line `should be equal to` "M4"
    }

}
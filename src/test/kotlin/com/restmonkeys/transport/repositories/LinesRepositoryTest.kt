package com.restmonkeys.transport.repositories

import org.amshove.kluent.`should be equal to`
import org.junit.Before
import org.junit.Test

/**
 * Created by Andrei Chernyshev on 8/19/18.
 */
class LinesRepositoryTest {

    lateinit var repository: LinesRepository

    @Before
    fun setup() {
        repository = LinesRepository()
    }

    @Test
    fun getLineById() {
        val result = repository.getLineById(0).block()!!
        result.id `should be equal to` 0
        result.name `should be equal to` "M4"
    }

    @Test
    fun getLineByName() {
        val result = repository.getLineByName("S75").block()!!
        result.id `should be equal to` 2
        result.name `should be equal to` "S75"
    }
}
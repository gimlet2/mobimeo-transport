package com.restmonkeys.transport.repositories

import org.amshove.kluent.`should be equal to`
import org.junit.Before
import org.junit.Test

/**
 * Created by Andrei Chernyshev on 8/19/18.
 */
class StopsRepositoryTest {

    lateinit var repository: StopsRepository

    @Before
    fun setup() {
        repository = StopsRepository()
    }

    @Test
    fun getStopsByCoordinates() {
        val result = repository.getStopsByCoordinates(2 to 12).blockFirst()!!
        result.id `should be equal to` 9
    }
}
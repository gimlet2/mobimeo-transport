package com.restmonkeys.transport.repositories

import com.restmonkeys.transport.dto.Stop
import org.amshove.kluent.`should be equal to`
import org.junit.Before
import org.junit.Test

/**
 * Created by Andrei Chernyshev on 8/19/18.
 */
class TimesRepositoryTest {

    lateinit var repository: TimesRepository

    @Before
    fun setup() {
        repository = TimesRepository()
    }

    @Test
    fun getLineIdByStopAndTime() {
        val result = repository.getLineIdByStopAndTime(Stop(0, 1, 1), "10:00:00").blockFirst()!!
        result `should be equal to` 0
    }
}
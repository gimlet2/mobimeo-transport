package com.restmonkeys.transport.services

import com.nhaarman.mockito_kotlin.whenever
import com.restmonkeys.transport.dto.Delay
import com.restmonkeys.transport.dto.Line
import com.restmonkeys.transport.dto.Stop
import org.amshove.kluent.`should be equal to`
import org.amshove.kluent.`should equal`
import org.amshove.kluent.mock
import org.junit.Before
import org.junit.Test
import reactor.core.publisher.Mono
import reactor.core.publisher.toFlux
import reactor.core.publisher.toMono

/**
 * Created by Andrei Chernyshev on 8/19/18.
 */
class LinesServiceTest {

    lateinit var service: LinesService

    @Before
    fun setup() {
        service = LinesService(mock(), mock(), mock(), mock())
    }

    @Test
    fun linesAt() {
        // setup
        val coordinates = 5 to 10
        val stop = Stop(1, coordinates.first, coordinates.second)
        val time = "10:00:00"
        val lineId = 5
        val expectedLine = Line(lineId, "M34")
        whenever(service.stopsRepository.getStopsByCoordinates(coordinates))
                .thenReturn(listOf(stop).toFlux())
        whenever(service.timesRepository.getLineIdByStopAndTime(stop, time)).thenReturn(listOf(lineId).toFlux())
        whenever(service.linesRepository.getLineById(5)).thenReturn(expectedLine.toMono())

        // act
        val result = service.lineAt(time, coordinates).blockFirst()

        // verify
        result `should equal` expectedLine
    }

    @Test
    fun isDelayed() {
        // setup
        val line = Line(23, "M54")
        whenever(service.linesRepository.getLineByName(line.name)).thenReturn(line.toMono())
        whenever(service.delaysRepository.getDelayByLine(line)).thenReturn(Delay(line.name, 32).toMono())

        // act
        val result = service.isDelayed(line.name).block()!!

        // verify
        result `should be equal to` true

    }

    @Test
    fun `isDelayed - not delayed`() {
        // setup
        val line = Line(23, "M54")
        whenever(service.linesRepository.getLineByName(line.name)).thenReturn(line.toMono())
        whenever(service.delaysRepository.getDelayByLine(line)).thenReturn(Delay(line.name, 0).toMono())

        // act
        val result = service.isDelayed(line.name).block()!!

        // verify
        result `should be equal to` false
    }

    @Test
    fun `isDelayed - not delay records`() {
        // setup
        val line = Line(23, "M54")
        whenever(service.linesRepository.getLineByName(line.name)).thenReturn(line.toMono())
        whenever(service.delaysRepository.getDelayByLine(line)).thenReturn(Mono.empty())

        // act
        val result = service.isDelayed(line.name).block()!!

        // verify
        result `should be equal to` false
    }
}